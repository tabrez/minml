
from pandas import read_csv
import numpy as np
from logreg import *
import ipdb

def load_and_prep_data(path):
    df = read_csv(path)
    df = df.replace({'Iris-setosa': 0,
                     'Iris-versicolor': 1,
                     'Iris-virginica': 1})
    df = df.values
    X, y, W, b = init_all(df)
    X, X_val, y, y_val = train_validate_split(X, y, 0.7)
    return (X, y, X_val, y_val, W, b)

X, y, X_val, y_val, W, b = load_and_prep_data('data/iris.csv')

# Wf, bf, C = gradient_descent(X, y, W, b, 1800, 0.5, True)
# alphas = [0.01, 0.1, 0.5, 1., 2., 5., 10., 15., 20.]
# alphas = [13, 15, 17, 20]
# alphas = [14, 15, 15.5, 16]
alphas = [15.32, 15.33, 15.34, 15.35, 15.4, 15.45, 15.5]
# print('cost vs. learning rate: \n',
#       np.array(cost_vs_learning_rates(X, y, W, b, alphas, 2000)))

# an example of the worst code in the history of the universe
def predict(X, W, b):
    return sigmoid(compute_Z(X, W, b))

def load_and_prep_data_mc(path):
    df = read_csv(path)

    dfse = df.replace({'Iris-setosa': 1,
                       'Iris-versicolor': 0,
                       'Iris-virginica': 0})
    dfse = dfse.values
    X, y, W, b = init_all(dfse)
    # X, X_val, y, y_val = train_validate_split(X, y, 0.7)
    Wfse, Bfse, _ = gradient_descent(X, y, W, b, 1800, 15, False)
    yp = np.zeros((3, y.shape[1]))
    yp[0,:] = predict(X, Wfse, Bfse)
    # print(yp.shape)

    dfve = df.replace({'Iris-setosa': 0,
                       'Iris-versicolor': 1,
                       'Iris-virginica': 0})
    dfve = dfve.values
    X, y, W, b = init_all(dfve)
    # X, X_val, y, y_val = train_validate_split(X, y, 0.7)
    Wfve, Bfve, _ = gradient_descent(X, y, W, b, 1800, 15, False)
    yp[1,:] = predict(X, Wfve, Bfve)
    # print(yp.shape)

    dfvi = df.replace({'Iris-setosa': 0,
                       'Iris-versicolor': 0,
                       'Iris-virginica': 1})
    dfvi = dfvi.values
    X, y, W, b = init_all(dfvi)
    # X, X_val, y, y_val = train_validate_split(X, y, 0.7)
    Wfvi, Bfvi, _ = gradient_descent(X, y, W, b, 1800, 15, False)
    yp[2,:] = predict(X, Wfvi, Bfvi)
    # print(yp.shape)

    res = np.argmax(yp, axis=0)
    res = res.reshape(1, len(res))
    # print(res.shape)

    # calculate training accuracy
    df = df.replace({'Iris-setosa': 0,
                     'Iris-versicolor': 1,
                     'Iris-virginica': 2})
    df = df.values
    X, y, W, b = init_all(df)
    correct = res == y
    print('Training accuracy: \n', np.sum(correct) / y.shape[1])
