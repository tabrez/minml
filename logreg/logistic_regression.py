
X, y, W, b = init_all(df)

# print(X.shape, y.shape)
X, X_val, y, y_val = train_validate_split(X, y, 0.7)
# print(X.shape, y.shape, X_val.shape, y_val.shape)

def plot_cost_vs_learning(df):
  df = DataFrame(np.stack((ca_nda[:,0], ca_nda[:,1]), axis=1),
                 columns=['alpha', 'cost'])
  plot = (pn.ggplot(df, pn.aes('alpha', 'cost'))
          + pn.geom_point()
          + pn.geom_line())
  return plot
