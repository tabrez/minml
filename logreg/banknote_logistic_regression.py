
import numpy as np
from pandas import read_csv
from logreg import *

def load_and_prep_data(path):
    df = read_csv(path)
    df = df.iloc[1:,:].values
    X, y, W, b = init_all(df)
    X, X_val, y, y_val = train_validate_split(X, y, 0.7)
    return (X, y, X_val, y_val, W, b)

X, y, X_val, y_val, W, b = load_and_prep_data('data/bank_note_data.csv')

alphas = [0.01, 0.1, 0.5, 1., 2., 5., 10., 15., 20.]
# alphas = [13, 15, 17, 20]
# alphas = [14, 15, 15.5, 16]
# alphas = [15.31, 15.32, 15.33, 15.34, 15.35, 15.4, 15.45, 15.5, 15.55, 15.6]
print('cost vs. learning rates: \n',
      np.array(cost_vs_learning_rates(X, y, W, b, alphas, 2000)))

Wf, bf, C = gradient_descent(X, y, W, b, 1800, 100, False)
print('Our accuracy: ',
      formatted_train_validate_accuracy(X, y, X_val, y_val, Wf, bf))
