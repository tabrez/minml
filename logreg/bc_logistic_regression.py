
from logreg import *

df = load_dataset('data/bc.csv')
df[:5]

X, y, W, b = init_all(df)

# print(X.shape, y.shape)
X, X_val, y, y_val = train_validate_split(X, y, 0.7)
# print(X.shape, y.shape, X_val.shape, y_val.shape)

X1 = np.array([[1,2],[3,4],[5,7]])
y1 = np.array([[1, 0]])
W1 = np.array([[1, 1, 1]])
b1 = 1
print('X shape:, ', X1.shape, 'y Shape: ', y1.shape, 'W1 shape: ', W1.shape)
print('cost of toy dataset: ', compute_cost(X1, y1, W1, b1))

# The error can be anything as we're using random values for W and b
print('cost of bc dataset:', compute_cost(X, y, W, b))

Wf, bf, _ = gradient_descent(X, y, W, b, 1800, 0.5, True)
print('cost after gradient descent:', compute_cost(X, y, Wf, bf))

# plot_cost_vs_iterations(X, y, W, b)

alphas = [0.01, 0.1, 0.5, 1., 2., 5.]
ca_nda = np.array(cost_vs_learning_rates(X, y, W, b, alphas))
print('cost vs. learning rates: \n', ca_nda)

# print(plot_cost_vs_learning(df)

print('Our accuracy: ',
      formatted_train_validate_accuracy(X, y, X_val, y_val, Wf, bf))

print('sklearn accuracy: ', sklearn_accuracy(X, y, X_val, y_val))
