
import numpy as np
from pandas import read_csv
from logreg import *

def load_and_prep_data(path):
    df = read_csv(path)
    df = df.iloc[1:,:].values
    indices = list(range(1, df.shape[1])) + [0]
    df = df[:, indices]
    # print(np.bincount(df[:,-1]))
    df[df[:,-1] != 1] = 0
    # print(np.bincount(df[:,-1]))
    # print(df.shape, np.unique(df[:,-1]))
    X, y, W, b = init_all(df)
    X, X_val, y, y_val = train_validate_split(X, y, 0.9)
    return (X, y, X_val, y_val, W, b)

X, y, X_val, y_val, W, b = load_and_prep_data('data/mnist/train.csv')
def try_cost_vs_learning():
    pass
    # alphas = [0.01]
    # alphas = [13, 15, 17, 20]
    # alphas = [14, 15, 15.5, 16]
    # alphas = [15.32, 15.33, 15.34, 15.35, 15.4, 15.45, 15.5, 15.55, 15.6]
    # print(cost_vs_learning_rates(X, y, W, b, alphas, 2000))

def try_gradient_descent():
  Wf, bf, _ = gradient_descent(X, y, W, b, 4, 1000)
  yp = predict(X_val, Wf, bf)
  print('y: \n:', np.bincount(np.squeeze(y_val)), '\n\ny(predicted): \n', np.bincount(np.squeeze(yp)))
  # print('Our accuracy: ',
  #       formatted_train_validate_accuracy(X, y, X_val, y_val, Wf, bf))
  # print('Costs: \n', np.array(C))
try_gradient_descent()
