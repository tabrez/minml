import n_layer_classification_nn as nn
import numpy as np
import pytest


@pytest.fixture
def s():
    '''Setup State object with example data.'''
    return nn.State([6, 3],
                    init_scheme='custom',
                    W=[np.array([[1, 2, 3, 4, 1, 2],
                                 [1, 1, 1, 1, 1, 1],
                                 [-1, -1, -1, -1, -1, -1]])],
                    b=[np.array([[10], [11], [-10]])])


def test_state_init(s):
    '''Test if all the fields of State object are correctly initialized.'''
    assert s.layer_count == 1
    assert s.unit_counts == [6, 3]
    assert len(s.weights) == 1
    assert len(s.biases) == 1
    assert [s.weights[i].shape
            for i in range(len(s.weights))] == [(3, 6)]
    assert [s.biases[i].shape
            for i in range(len(s.biases))] == [(3, 1)]


def test_feed_forward(s):
    '''Test if feed_forward computes all A's and Z's correctly.'''
    X = np.array([[3], [1], [4], [2], [6], [5]])
    Z_expected = np.array([[51], [32], [-31]])

    _, A, Z = nn.feed_forward(X, s)
    # TODO: why `np.array_equal` doesn't work?
    # why are there `\n` characters in arrays? check dtype
    assert np.array_equiv(Z, Z_expected)
    assert np.array_equiv(A, nn.sigmoid(Z_expected))
