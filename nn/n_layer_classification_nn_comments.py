
'''This module implements a basic neural netowrk for classification problems.
'''

import numpy as np
import ipdb

class State:
    '''
    We store the unit count of each layer and corresponding weights and biases
    in State object.

    >>> state = State([4, 5, 1])

    >>> state.layer_count
    2

    >>> state.unit_counts
    [4, 5, 1]

    >>> len(state.weights)
    2

    >>> [state.weights[i].shape for i in range(len(state.weights))]
    [(5, 4), (1, 5)]

    >>> len(state.biases)
    2

    >>> [state.biases[i].shape for i in range(len(state.biases))]
    [(5, 1), (1, 1)]
    '''

    def init_with_random(self, unit_counts):
        np.random.seed(42)
        w = [np.random.rand(j, k) for j, k
             in zip(unit_counts[1:], unit_counts)]
        b = [np.random.rand(j, 1) for j
             in unit_counts[1:]]
        return (w, b)

    def init_with_zeros(self, unit_counts):
        w = [np.zeros(j, k) for j, k
             in zip(unit_counts[1:], unit_counts)]
        b = [np.zeros(j, 1) for j
             in unit_counts[1:]]
        return (w, b)

    def __init__(self, unit_counts, init_scheme='random', W=None, b=None):
        self.layer_count = len(unit_counts) - 1
        self.unit_counts = unit_counts
        if init_scheme == 'random':
            self.weights, self.biases = self.init_with_random(unit_counts)
        elif init_scheme == 'custom':
            self.weights, self.biases = W, b
        else:
            self.weights, self.biases = self.init_with_zeros(unit_counts)

def sigmoid(z):
    '''The sigmoid function for scalars and ndarrays.

    The `sigmoid` function, also called the logistic function, computes and
    returns 1/(1+e^(-z). It's available in SciPy as `scipy.special.expit()`

    Parameters
    ----------
    z : array_like
        Any scalar or ndarray to apply sigmoid to. Applied element-wise
        for ndarray.

    Returns
    -------
    out : array_like
        An ndarray of the same shape as x. Its entries are sigmoid of the
        corresponding entry of z. This is a scalar if z is a scalar.

    Examples
    --------
    >>> np.round(sigmoid(-10), 2)
    0.0
    >>> np.round(sigmoid(0), 2)
    0.5
    >>> np.round(sigmoid(10), 2)
    1.0
    >>> np.round(sigmoid(np.array([-10, -2, 0, 2, 10])), 2)
    array([0.  , 0.12, 0.5 , 0.88, 1.  ])

    '''

    return 1. / (1 + np.exp(-z))


def sigmoid_prime(z):
    '''The gradient of sigmoid function for scalars and ndarrays.

    Parameters
    ----------
    z : array_like
        Any scalar or ndarray.

    Returns
    -------
    out : array_like
        An ndarray of the same shape as x. Its entries are gradient of the
        sigmoid of the corresponding entry of z.
        This is a scalar if z is a scalar.

    Examples
    --------
    >>> np.round(sigmoid(-10), 2)
    0.0
    >>> np.round(sigmoid(0), 2)
    0.5
    >>> np.round(sigmoid(10), 2)
    1.0
    >>> np.round(sigmoid(np.array([-10, -2, 0, 2, 10])), 2)
    array([0.  , 0.12, 0.5 , 0.88, 1.  ])

    '''

    return sigmoid(z) * (1 - sigmoid(z))

def feed_forward(X, state, cache=False):
    '''Computes one full feed-forward propagation of input through a
    neural network.

    In a neural network, X is sometimes referred to as A[0] and the layers are
    numbered from l=1 to L. In the program we will number the layers from
    l = 0 to L because of zero-based indexing in Python. A[0] will be set to
    X and W[0], b[0] & Z[0] be set to None.

    Parameters
    ----------
    X : input ndarray of shape `(n, m)` with `n` featuers per observation and
    `m` observations
    state : contains the following fields -
        - `state.weights` & `state.biases` : initial weights and biases
        - `state.layer_count` : total number of layers in the neural network
        - `state.unit_counts` : list of count of units in each layer of the
           neural network

    Examples
    --------
    # one sample, m = 1; one output
    >>> s = State([6, 3, 1])

    >>> X = np.array([[3], [2], [6], [1], [1], [8]])
    >>> _, A, Z = feed_forward(X, s, cache=True)

    >>> len(A)
    2
    >>> len(Z)
    2
    >>> [ai.shape for ai in A]
    [(3, 1), (1, 1)]
    >>> [zi.shape for zi in Z]
    [(3, 1), (1, 1)]

    # multiple samples, m = 4; one output

    >>> s = State([6, 3, 1])

    >>> X = np.array(
    ... [[3, 2, 1, 1],
    ... [2, 2, 11, -3],
    ... [6, 13, -1, 22],
    ... [1, 21, 12, 11],
    ... [1, -5, 15, -15],
    ... [8, -21, 7, -29]])

    >>> _, A, Z = feed_forward(X, s, cache=True)

    >>> len(A)
    2
    >>> len(Z)
    2
    >>> [ai.shape for ai in A]
    [(3, 4), (1, 4)]
    >>> [zi.shape for zi in Z]
    [(3, 4), (1, 4)]
    '''
    a = X
    ac = []
    zc = []

    for wl, bl in zip(state.weights,
                      state.biases):
        z = np.dot(wl, a) + bl
        a = sigmoid(z)
        if cache:
            zc.append(z)
            ac.append(a)

    return (a, ac, zc)

def predict(X, s):
    '''Predict 0 or 1 for each observation in X given a matrix of input X and a
    set of learned parameters in X.

    Parameters:
    -----------
    X : array_like
    Input with dimensions (n, m) where n is no. of features and m is no. of
    observations.
    s : State
    State object containing number of layers, weights, biases.

    Examples:
    ---------
    >>> X = np.array([[3], [1], [4], [2], [6], [5]])

    >>> s = State([6, 3, 1],
    ...            init_scheme='custom',
    ...            W=[np.array([[1, 2, 3, 4, 1, 2],
    ...                          [1, 1, 1, 1, 1, 1],
    ...                          [-1, -1, -1, -1, -1, -1]]),
    ...               np.array([2, 3, 2])],
    ...             b=[np.array([[10], [11], [-10]]), np.array([4])])

    >>> predict(X, s)
    [1]
    '''
    A,_,_ = feed_forward(X, s, cache=False)
    return [1 if a > 0.5 else 0 for a in A[0]]

def compute_cost(yp, y):
    '''Return the number of instances where yp doesn't equal to y.

    Parameters:
    -----------
    yp: array_like
    A 1D array; predicted values, each value should be either 0 or 1.

    y: array_like
    A 1D array; actual output values, each value should be either 0 or 1.

    Returns:
    -------
    return np.sum(np.abs(yp - y))

    Examples:
    ---------
    >>> compute_cost(np.array([0, 1, 1, 0, 1, 1, 1]),
    ...              np.array([1, 1, 0, 0, 0, 1, 0]))
    4
    >>> compute_cost(1, 0)
    1
    >>> compute_cost(np.array([1]), np.array([0]))
    1
    '''
    return np.sum(np.abs(yp - y))

def compute_gradients(X, Y, A, Z, s):
    '''Compute the gradients for the cost function w.r.t W1 and b1.

    Parameters:
    -----------
    y: array_like
    A 1D array; actual output values, each value should be either 0 or 1.
    A: array_like
    A list of all the activation output vectors at each layer of the neural
    network
    Z: A list of all the output vectors at each layer of the neural network
    W: A list of all the weight matrices at each layer of the neural network

    Returns:
    --------
    A tuple of gradients of cost function w.r.t. W1 and b1
    '''
    L = s.layer_count - 1
    W = s.weights

    dCdA = dCdZ = [np.zeros_like(a) for a in A]
    dCdW = [np.zeros_like(w) for w in W]
    dCdB = [np.zeros_like(b) for b in s.biases]

    dCdA[L] = [(-y / a) + ((1 - y) / (1 - a + 1e-15)) for (y, a) in zip(Y, A[L])]
    dCdZ[L] = dCdA[L] * sigmoid_prime(Z[L])

    for li in reversed(range(L+1)):
        if li != L:
            dCdZ[li] = (W[li+1].T @ dCdZ[li+1]) * sigmoid_prime(Z[li])
        dCdW[li] = dCdZ[li] @ (X.T if li ==0 else A[li-1].T)
        dCdB[li] = np.sum(dCdZ[li], axis=1, keepdims=True)

    return (dCdW, dCdB)

def gradient_descent(X, s, Y, k, epochs, alpha, cost_hist=False):
    '''Learn the values for parameters W & b using input in X and output in y
    using gradient_descent algorithm.

    Parameters:
    -----------
    X: array_like
    Input with dimensions (n, m) where n is no. of features and m is no. of
    observations.
    state: State object with initial values for weight and bias parameters
    y: array_like
      A 1D array; actual output values, each value should be either 0 or 1.
    k: number of observations to use from X for each epoch
    epoch: number of gradient descent iterations to perform
    alpha: learning rate

    Returns:
    --------
    Learned values of weight and bias parameters after `epoch` iterations
    '''

    m = X.shape[1]
    ch = []

    for epoch in range(epochs):
        for start in range(0, m, k):
            if cost_hist and epoch % 100 == 0:
                ch.append(compute_cost(predict(X, s), Y))
            end = start+k if start+k <= m else m
            mini_batch_X = X[:, start:end]
            mini_batch_Y = Y[:, start:end]
            _, A, Z = feed_forward(mini_batch_X, s, cache=True)
            delW, delB = compute_gradients(mini_batch_X,
                                                 mini_batch_Y,
                                                 A, Z, s)
            s.weights = [w - t2 for (w, t2) in zip(s.weights,
                                           [alpha * delw for delw in delW])]
            s.biases = [b - t2 for (b, t2) in zip(s.biases,
                                           [alpha * delb for delb in delB])]
    return (s.weights, s.biases, ch)

def try_toy_dataset():
    # X = np.array([[3], [1], [4], [2], [6], [5]])
    X = np.array([[3,1,2], [1,1,2], [4,1,2], [2,1,2], [6,1,2], [5,1,2]])
    y = np.array([[1, 0, 0]])
    s = State([6, 3, 1],
              init_scheme='custom',
              W=[np.array([[1, 2, 3, 4, 1, 2],
                           [1, 1, 1, 1, 1, 1],
                           [-1, -1, -1, -1, -1, -1]]),
                 np.array([[2, 3, 2]])],
              b=[np.array([[10], [11], [-10]]), np.array([4])])
    #_, A, Z = feed_forward(X, state, cache=True)
    s.weights, s.biases = gradient_descent(X,
                                           state,
                                           y,
                                           k=2,
                                           epochs=2,
                                           alpha=0.001)
    return feed_forward(X, state, cache=False)[0]

def load_dataset(path):
    from pandas import read_csv
    df = read_csv(
        path,
        header=None,
        dtype=np.float32,
        na_values='?',
        usecols=[1,2,3,4,5,7,8,9,10]
    )
    return df.values

def accuracy(X, y, s):
    A = predict(X, s)
    correct = A == y
    return np.sum(correct) / y.shape[1]

def try_bc_dataset():
    df = load_dataset('data/bc.csv')
    X = df[:, :-1].T
    y = df[np.newaxis, :, -1]
    s = State([X.shape[0], 3, 1])
    s.weights, s.biases, ch = gradient_descent(X, s, y,
                                               k=X.shape[1],
                                               epochs=1800,
                                               alpha=0.0015,
                                               cost_hist=True)
    print('accuracy:', accuracy(X, y, s))
    return predict(X, s)

if __name__ == '__main__':
    import doctest
    doctest.testmod()

    # print(try_toy_dataset())

    res = try_bc_dataset()
    print(res)
