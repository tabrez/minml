from linreg import *
import numpy as np
import math
import pytest

@pytest.fixture
def init_vars():
    x1 = np.array([1., 2., 3., 4., 5., 6., 7., 8., 9., 10.])
    x2 = np.array([1.1, 2.3, 3.6, 4.1, 5.2, 6.1, 7.3, 8.6, 9.1, 9.2])
    l = np.array([1., 2., 1.3, 3.75, 2.25, 5., 7., 6, 8, 10.3])
    df = np.column_stack((x1, x2, l))
    return init_all(df) + (df,)

def test_init_all(init_vars):
    X, y, W, b, df = init_vars
    assert X.shape[1] == df.shape[0]
    assert y.shape[1] == df.shape[0]
    assert X.shape[0] == df.shape[1] - 1
    assert y.shape[0] == 1

    assert np.allclose(X[0], df[:, 0].T)
    assert np.allclose(X[1], df[:, 1].T)
    assert np.allclose(y, df[:, 2].T)

    assert W.shape == (1, X.shape[0])
    assert isinstance(b, float)

def test_split_shapes(init_vars):
    X, y, _, _, _  = init_vars
    pct = 0.7
    X_tr, X_val, y_tr, y_val = train_validate_split(X, y, pct)
    # print('len X: ', X.shape[1], 'len X_tr: ', X_tr.shape[1])
    assert X_tr.shape[0] == X.shape[0]
    assert y_tr.shape[0] == y.shape[0]
    assert X_val.shape[0] ==  X.shape[0]
    assert y_val.shape[0] ==  y.shape[0]

    # might fail with very small X.shape[1] length; increase `tol` in that case
    assert math.isclose(X_tr.shape[1], pct * X.shape[1])
    assert math.isclose(y_tr.shape[1], pct * y.shape[1])
    assert math.isclose(X_val.shape[1], (1-pct) * X.shape[1])
    assert math.isclose(y_val.shape[1], (1-pct) * y.shape[1])

def test_split_randomised(init_vars):
    X, y, _, _, _ = init_vars
    pct = 0.7
    X_tr, X_val, y_tr, y_val = train_validate_split(X, y, pct)
    assert not np.array_equal(X[0, :5], X_tr[0, :5])
    assert not np.array_equal(X[0, :-5], X_tr[0, :-5])
    assert not np.array_equal(X[0, :5], X_val[0, :5])
    assert not np.array_equal(X[0, :-5], X_val[:-5, :-5])

    assert not np.array_equal(y[0, :5], y_tr[0, :5])
    assert not np.array_equal(y[0, :-5], y_tr[0, :-5])
    assert not np.array_equal(y[0, :5], y_val[0, :5])
    assert not np.array_equal(y[0, :-5], y_val[0, :-5])

    assert not np.array_equal(X[1, :5], X_tr[1, :5])
    assert not np.array_equal(X[1, :-5], X_tr[1, :-5])
    assert not np.array_equal(X[1, :5], X_val[1, :5])
    assert not np.array_equal(X[1, :-5], X_val[:-5, :-5])

    # how to test if row/vertical order is preserved while columns are shuffled
