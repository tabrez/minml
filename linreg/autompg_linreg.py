
from linreg import *

def load_dataset(path, cols):
    df = read_csv(path, dtype=np.float32, usecols=cols, na_values='?').values
    df = df[~np.isnan(df).any(axis=1)]
    return df[:, [1, 2, 3, 4, 5, 0]]

df = load_dataset('../data/auto-mpg.csv', cols=[0,1,2,3,4,5])
#print(d.isnull().sum())
#print('d: ', d.shape, d[:5,:])

# move first(target) column to last position, drop 4th
#print(df.shape, df[:5,:])

X, X_val, y, y_val, W, b = prep_data(df, 0.7)
#print(X.shape, y.shape, W.shape, b)
print('cost(pre-training): ', compute_cost(X, y, W, b))

Wf, bf, C = gradient_descent(X, y, W, b, 0.0000002, 350000, False)

print('cost(post-training) :', compute_cost(X, y, Wf, bf))

print(compare_r_2(X, y, X_val, y_val, Wf, bf))

print(sklearn_ols_r_2(X, y, X_val, y_val))

print(sklearn_ridge_r_2(X, y, X_val, y_val))

