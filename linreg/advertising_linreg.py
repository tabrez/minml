from linreg import *


def load_dataset(path, cols):
    df = read_csv(path, dtype=np.float32, usecols=cols, na_values='?')
    #print((df.shape, df.head()))
    #print(d.values[:, 1:])
    return df.values


def try_toy_dataset():
    X = np.array([[1,2],[3,4],[5,7]])
    y = np.array([[1, 0]])
    W = np.array([[1, 1, 1]])
    b = 1
    return (X, y, W, b)

# The error can be anything as we're using random values for W and b
print('cost(toy dataset): ', compute_cost(*try_toy_dataset()))

df = load_dataset('data/advertising.csv', cols=[1,2,3,4])
X, X_val, y, y_val, W, b = prep_data(df, 0.7)
# error with random values for W and b
print('cost(pre-training): ', compute_cost(X, y, W, b))

# slowly converge to suitable learning rates starting at a a broader range
# alphas = [1e-10, 1e-5, 1e-1, 1e5, 1e10]
# alphas = [0.000001, 0.000005, 0.000009, 0.00001, 0.00002, 0.00003, 0.0000657]
# print('cost vs learning rates: ',
#       np.array(cost_vs_learning_rates(X, y, W, b, alphas, iters=2900)))
# p = plot_cost_vs_learning_rate(X, y, W, b, alphas, iters=900)
# p.save('images/cost_vs_alpha.png', width=15, height=8)
# print(p)

# p = plot_cost_vs_iters(X, y, W, b, alpha=0.00001, iters=900)
# p.save('images/cost_vs_iters.png', width=15, height=8)
# print(p)

Wf, bf, _ = gradient_descent(X, y, W, b, 0.00001, 1300, False)
# error after learning W and b
print('cost(post-training) :', compute_cost(X, y, Wf, bf))

print(compare_r_2(X, y, X_val, y_val, Wf, bf))

print(sklearn_ols_r_2(X, y, X_val, y_val))

print(sklearn_ridge_r_2(X, y, X_val, y_val))
