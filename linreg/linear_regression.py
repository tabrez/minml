
def TV_vs_sales_plot(df):
    return (pn.ggplot(df, pn.aes('TV', 'sales'))
         + pn.geom_point())

df = read_csv('data/advertising.csv',
              dtype=np.float32,
              usecols=[1,2,3,4],
              na_values='?')
p = TV_vs_sales_plot(df)
p.save('images/tv_vs_sales.png', width=15, height=8)
# print(p)
