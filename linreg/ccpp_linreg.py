
from linreg import *
from pandas import concat
import glob

#df = read_csv('data/ccpp_sheet1.csv')

### import os for making this platform independent
def load_data(path):
    files = glob.glob(path)
    return concat((read_csv(f) for f in files)).values
#print(a.isnull().sum())
#print('df: ', df.shape, df[:5])
df = load_data('../data/ccpp*.csv')
X, X_val, y, y_val, W, b = prep_data(df, 0.95)
#print(X.shape, y.shape, X_val.shape, y_val.shape, W.shape, b)
print('cost(pre-training): ', compute_cost(X, y, W, b))

Wf, bf, C = gradient_descent(X, y, W, b, 0.00000193, 19000, False)

print('cost(post-training) :', compute_cost(X, y, Wf, bf))

print(compare_r_2(X, y, X_val, y_val, Wf, bf))

print(sklearn_ols_r_2(X, y, X_val, y_val))

print(sklearn_ridge_r_2(X, y, X_val, y_val))
